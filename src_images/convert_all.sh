#!/bin/sh

WIDTH=640
HEIGHT=640
OUTPUT_DIR="raster/"

[ -d $OUTPUT_DIR ] || mkdir $OUTPUT_DIR

for file in *.svg;
do
  OUTPUT_FILE="$(basename "$file" .svg).png"
  inkscape --export-background "#000000" -f "$file" -w $WIDTH -h $HEIGHT -e "$OUTPUT_DIR/$OUTPUT_FILE"
done
