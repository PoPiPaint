#!/usr/bin/env python
#
# PoPiPaint - Polar Pixel Painter, a tool to draw polar patterns
#
# Copyright (C) 2014  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import wx
import CanvasFrame


class PoPiPaApp(wx.App):
    def __init__(self, *args, **kwargs):
        self.base_image = kwargs.pop('base_image', None)
        wx.App.__init__(self, *args, **kwargs)

    def OnInit(self):
        # We do not want a resizeable frame!
        framestyle = wx.DEFAULT_FRAME_STYLE & \
            ~(wx.RESIZE_BORDER | wx.MAXIMIZE_BOX)
        frame = CanvasFrame.CanvasFrame(None, title="PoPiPaint", style=framestyle,
                                        base_image=self.base_image)
        frame.Show()
        return True


if __name__ == "__main__":
    if len(sys.argv) > 1:
        app = PoPiPaApp(base_image=sys.argv[1])
    else:
        app = PoPiPaApp()

    app.MainLoop()
