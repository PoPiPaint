#!/bin/sh
#
# depolar_resample_IMAGEMAGICK.sh - build a pixel map for JMPrope
#
# Copyright (C) 2014  Antonio Ospite <ao2@ao2.it>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

set -e

INPUT_WIDTH=640
INPUT_HEIGHT=640
SCALE="0.1"
INTERNAL_RADIUS=2

[ -f "$1" -a "x$2" != "x" ] || { echo "usage: $(basename $0) <input file> <output file> [imagemagick options]" 1>&2; exit 1; }

INPUT_FILENAME="$1"
shift

OUTPUT_FILENAME="$1"
shift

[ -e $OUTPUT_FILENAME ] && { echo "Error: $OUTPUT_FILENAME already exists, remove it first" 1>&2; exit 1; }

WIDTH=$(identify -format "%[fx:w]" "$INPUT_FILENAME")
HEIGHT=$(identify -format "%[fx:w]" "$INPUT_FILENAME")

if [ $WIDTH -ne $INPUT_WIDTH -o $HEIGHT -ne $INPUT_HEIGHT ];
then
  echo "Error: image must be ${INPUT_WIDTH}x${INPUT_HEIGHT}" 1>&2
  exit 1;
fi

CX=$(identify -format "%[fx:h/2]" "$INPUT_FILENAME")
CY=$(identify -format "%[fx:h/2]" "$INPUT_FILENAME")

OPTIONS="-interpolate NearestNeighbor -filter point -background black -flatten"

convert "$INPUT_FILENAME" $OPTIONS \
  -set option:distort:scale $SCALE +distort DePolar "0 0 $CX,$CY 0,360" \
  -flop -crop +0+$INTERNAL_RADIUS $@ +repage "$OUTPUT_FILENAME"
