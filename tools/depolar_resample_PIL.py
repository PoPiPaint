#!/usr/bin/env python
#
# Copyright (C) 2014  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from math import *

import Image


def polar2cartesian(r, theta, offset_angle=0):
    """
    theta expected in degrees
    """
    theta = radians(theta) - offset_angle
    x = r * cos(theta)
    y = r * sin(theta)

    return x, y


def usage(program_name):
    sys.stderr.write("usage: %s <input file> <output file>\n" % program_name)
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage(sys.argv[0])

    map_width = 101
    map_height = 30
    internal_radius = 2
    scale = 10

    width = (map_height + internal_radius) * 2 * scale
    height = width

    img = Image.open(sys.argv[1]).convert('RGBA')
    w, h = img.size
    if w != width or h != height:
        sys.stderr.write("Error: image must be 640x640")

    colors = img.getcolors(w*h)
    palette = img.getpalette()
    pixels = img.getdata()

    output_image = Image.new("RGB", (map_width, map_height), "black")

    external_radius = width / 2. - 1
    radius = external_radius - internal_radius

    for i in range(0, map_height):
        for j in range(0, map_width):
            r = radius / map_height * (i + 0.5) + internal_radius
            theta = (360. / map_width) * (j + 0.5)
            x, y = polar2cartesian(r, theta, -pi/2.)
            px, py = int(x + radius), int(y + radius)
            sample = img.getpixel((px, py))
            if sample[3] != 255:
                sample = (0, 0, 0, 255)
            output_image.putpixel((j, i), sample)

    output_image.save(sys.argv[2], "PNG")
